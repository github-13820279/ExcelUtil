package com.lkx;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.lkx.model.PhoneModel;
import com.lkx.util.ExcelUtil;

@Controller
public class TestController {
	
	@GetMapping(value = "/")
	public String ss1(){
		return "index";
	}
	
	@PostMapping(value = "/test")
	@ResponseBody
	public static List<PhoneModel> testImport(MultipartFile file) throws Exception{
		List<PhoneModel> list = ExcelUtil.readXls(file.getBytes(), PhoneModel.class);
		return list;
	}

	/**
	 * @Function 注解测试
	 * @author   likaixuan
	 * @Date     2019-10-10 18:31
	 * @param     * @param null
	 * @return
	 */
	@PostMapping(value = "/test1")
	@ResponseBody
	public List<PhoneModel> testImport1(MultipartFile file) throws Exception{
		List<PhoneModel> list = ExcelUtil.readXls(file.getBytes(), PhoneModel.class);

		return list;
	}

	@GetMapping(value = "/export")
	public void testExport(HttpServletResponse response) throws Exception{
		 
		List<PhoneModel> list = new ArrayList<>();
		for(int i=0;i<10;i++){
			PhoneModel model = new PhoneModel();
			model.setNum((i+1));
			model.setColor("金色"+i);
			model.setPhoneName("苹果"+i+"S");
			model.setPrice(i);
			model.setSj(new Date());
			list.add(model);
		}
		ExcelUtil.exportExcelOutputStream(response,list,PhoneModel.class,"测试Excel导出");
	}

	//导出带表头
	@GetMapping(value = "/exportHeader")
	public void testExportHeader(HttpServletResponse response) throws Exception{

		List<PhoneModel> list = new ArrayList<>();
		for(int i=0;i<10;i++){
			PhoneModel model = new PhoneModel();
			model.setColor("金色"+i);
			model.setPhoneName("苹果"+i+"S");
			model.setPrice(i);
			model.setSj(new Date());
			list.add(model);
		}
		ExcelUtil.exportExcelOutputStream(response,list,PhoneModel.class,"导出标题多萨法撒旦法带表头",true);
	}

	//模版导出
	@GetMapping(value = "/exportTemplate")
	public void exportTemplate(HttpServletResponse response) throws Exception{

		PhoneModel model = new PhoneModel();
		model.setColor("金色");
		model.setPhoneName("苹果12 S");
		model.setPrice(9999);
		model.setSj(new Date());

		ExcelUtil.templateWrite(response,"E:\\新建XLSX 工作表.xlsx",model,"ce测试时");
	}
	
	@GetMapping(value = "/json")
	@ResponseBody
	public BizResult<PhoneModel> ss(){
		
		List<PhoneModel> list = new ArrayList<>();
		
		
		PhoneModel model = new PhoneModel();
		model.setColor("土豪金");
		model.setPhoneName("ipone X");
		model.setSj(new Date());
		
		list.add(model);
		
		BizResult<PhoneModel> result = new BizResult<>();
		result.setCode("0");
		result.setData(model);
		result.setList(list);
		
		return result;
	}
	
}
