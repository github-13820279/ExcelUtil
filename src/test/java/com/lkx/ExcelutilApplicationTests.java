package com.lkx;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.lkx.util.Excel;
import com.lkx.util.ExcelUtilBase;
import org.junit.Test;
import com.lkx.model.PhoneModel;
import com.lkx.util.ExcelParam;
import com.lkx.util.ExcelUtil;


public class ExcelutilApplicationTests {

	@Test
	public void contextLoads() throws Exception {
		
		String keyValue ="手机名称:phoneName,颜色:color,售价:price,时间:sj"; 
		ExcelParam param = new ExcelParam();
		param.setFilePath("D://testsss.xlsx");
		param.setMap(ExcelUtil.getMap(keyValue));
		param.setClazz(PhoneModel.class);
		//param.setSheetIndex(2);
		
		List<PhoneModel> list =  ExcelUtil.getResult(param);
		
	     for (PhoneModel phoneModel : list) {
			System.out.println(phoneModel);
		}

	     
	}

	/**
	 * 测试注解读取Excel
	 * @throws Exception
	 */
	@Test
	public void testAnnotationReadXls() throws Exception
	{

		List<PhoneModel> list =  ExcelUtil.readXls("D://testxxx.xls",PhoneModel.class);
		for (PhoneModel phoneModel : list) {
			System.out.println(phoneModel);
		}
	}
	
	/**
	 * 测试读取Excel,需要定义的字段和Excel中的表头完全匹配
	 * @throws Exception
	 */
	@Test
	public void testReadXls() throws Exception
	{
		String keyValue ="手机名称:phoneName,颜色:color,售价:price,时间:sj"; 
	     List<PhoneModel> list =  ExcelUtil.readXls("D://testsss.xlsx",ExcelUtil.getMap(keyValue),PhoneModel.class);
	     for (PhoneModel phoneModel : list) {
			System.out.println(phoneModel);
		}
	}
	
	/**
	 * 测试读取Excel,需要定义的字段
	 * @throws Exception
	 */
	@Test
	public void readXlsPart() throws Exception
	{
		List<PhoneModel> list = new ArrayList();
		Long startTime = System.currentTimeMillis();
		list =  ExcelUtil.readXls("D://chrome-downloads//导出标题多萨法撒旦法 (11).xlsx",PhoneModel.class,2);
		Long endTime = System.currentTimeMillis();
		System.out.println("读取"+list.size()+"耗时，"+(endTime-startTime)+"ms");

	}
	
	/**
	 * 用List模拟一个数据源，导出到本地磁盘
	 * @throws Exception
	 */
	@Test
	public void testExportExcel() throws Exception{
		List<PhoneModel> list = new ArrayList<PhoneModel>();
		PhoneModel model;
		for(int i=0;i<500;i++){
			model = new PhoneModel();
			model.setColor("金色"+i);
			model.setPhoneName("苹果"+i+"S");
			model.setPrice(i);
			model.setSj(new Date());
			list.add(model);
		}
		Long startTime = System.currentTimeMillis();
		//ExcelSXSSFUtil.exportExcel("d:/testsss.xlsx",list,PhoneModel.class);
		Long endTime = System.currentTimeMillis();
		System.out.println("导出"+list.size()+"耗时，"+(endTime-startTime)+"ms");
		startTime = System.currentTimeMillis();
		ExcelUtil.exportExcel("d:/testsss2.xlsx",list,PhoneModel.class);
		endTime = System.currentTimeMillis();
		System.out.println("导出"+list.size()+"耗时，"+(endTime-startTime)+"ms");
	}


	public static void main(String[] args) throws Exception {
		PhoneModel phoneModel = new PhoneModel();
		phoneModel.setSj(new Date());
		phoneModel.setNum(9);
		phoneModel.setColor("玫瑰金");
		phoneModel.setPrice(9999);
		phoneModel.setPhoneName("苹果12 pro max");
		ExcelUtilBase.getMap(phoneModel);

		ExcelUtil.templateWrite(null,"E:\\新建XLSX 工作表.xlsx","E:\\新建XLSX 工作表2.xlsx",phoneModel);
	}

}
