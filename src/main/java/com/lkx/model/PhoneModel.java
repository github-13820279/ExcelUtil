/**
 * Project Name:excelutil
 * File Name:PhoneModel.java
 * Package Name:com.lkx.util
 * Date:2017年6月7日上午9:43:19
 * Copyright (c) 2017~2020, likaixuan@test.com.cn All Rights Reserved.
 *
*/

package com.lkx.model;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Date;
import com.lkx.util.Excel;

/**
 * ClassName:PhoneModel
 * Function: TODO ADD FUNCTION.
 * Reason: TODO ADD REASON.
 * Date:     2017年6月7日 上午9:43:19
 * @author   likaixuan
 * @version  V1.0
 * @since    JDK 1.7
 * @see
 */
public class PhoneModel implements Serializable{

    @Excel(title = "序号")
    private int num;

    @Excel(title = "手机名称")
    private String phoneName;

    @Excel(title = "颜色")
    private String color;

    @Excel(title = "售价")
    private double price;

    @Excel(title = "时间")
    private Date sj;


    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getPhoneName() {
        return phoneName;
    }

    public void setPhoneName(String phoneName) {
        this.phoneName = phoneName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getSj() {
        return sj;
    }

    public void setSj(Date sj) {
        this.sj = sj;
    }


    /**
     * 解析字段注解
     *
     * @param clazz
     */
    public static <T> void parseField(Class<T> clazz) throws Exception {

        Field field;
        Field[] fields=clazz.getDeclaredFields();
        for (int i = 0; i <fields.length ; i++) {
            fields[i].setAccessible(true);
        }
        for (int i = 0; i <fields.length ; i++) {
            field=clazz.getDeclaredField(fields[i].getName());
            Excel column=field.getAnnotation(Excel.class);
            if(column!=null){
                System.out.println(fields[i].getName()+":"+column.title());
            }
        }
    }

    
}

